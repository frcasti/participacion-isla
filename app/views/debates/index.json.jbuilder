json.array!(@debates) do |debate|
  json.extract! debate, :id, :title, :content, :user_id
  json.url debate_url(debate, format: :json)
end
