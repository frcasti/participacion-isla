class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  helper_method :resumen

  include CanCan::ControllerAdditions
  
  protect_from_forgery with: :exception

  def resumen( str, n = 5 )
  	return ActionController::Base.helpers.strip_tags( (str.split[0...n].join(' ').length < str.length) ? str.split[0...n].join(' ') + '...' : str )
  end
end
