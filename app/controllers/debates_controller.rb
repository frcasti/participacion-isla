class DebatesController < ApplicationController
  before_action :set_debate, only: [:show, :edit, :update, :destroy]

  before_action :authenticate_user!, except: [:index, :show, :edit]

  load_and_authorize_resource

  # GET /debates
  # GET /debates.json
  def index
    @debates = Debate.all
  end

  # GET /debates/1
  # GET /debates/1.json
  def show
  end

  # GET /debates/new
  def new
    @debate = Debate.new
  end

  # GET /debates/1/edit
  def edit
  end

  # POST /debates
  # POST /debates.json
  def create
    params[:debate][:user_id] = current_user.id
    @debate = Debate.new(debate_params)

    respond_to do |format|
      if @debate.save
        # add tags after save debate
        params[:debate][:tags].each do |e|
          t = Tag.where(name: e).first
          if !t.nil?
            if !@debate.tags.include?(t)
              @debate.tags.push(t);
            end
          else
            @debate.tags.create(name: e) unless (e == '');
          end
        end
        format.html { redirect_to @debate, notice: 'Debate was successfully created.' }
        format.json { render :show, status: :created, location: @debate }
      else
        format.html { render :new }
        format.json { render json: @debate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /debates/1
  # PATCH/PUT /debates/1.json
  def update
    params[:debate][:tags].each do |e|
      t = Tag.where(name: e).first
      if !t.nil?
        if !@debate.tags.include?(t)
          @debate.tags.push(t);
        end
      else
        @debate.tags.create(name: e);
      end
    end
    respond_to do |format|
      if @debate.update(debate_params)
        format.html { redirect_to @debate, notice: 'Debate was successfully updated.' }
        format.json { render :show, status: :ok, location: @debate }
      else
        format.html { render :edit }
        format.json { render json: @debate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /debates/1
  # DELETE /debates/1.json
  def destroy
    @debate.destroy
    respond_to do |format|
      format.html { redirect_to debates_url, notice: 'Debate was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_debate
      @debate = Debate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def debate_params
      params.require(:debate).permit(:title, :content, :featured_image, :user_id, :tags)
    end
end
