class RatingsController < ApplicationController
	def set_debate_rating
		@rating = Rating.where(debate_id: params['debate_id'].to_i, user_id: current_user.id).first
		if @rating.nil?
			@rating = Rating.new(
				debate_id: params['debate_id'].to_i,
				up: (params['up'] == 'true') ? true : false,
				down: (params['down'] == 'true' && params['up'] == 'false') ? true : false,
				user_id: current_user.id)
			respond_to do |format|
	      if @rating.save
	        # format.html { redirect_to @rating, notice: 'rating was successfully created.' }
	        data = {created: 'ok',
	        	up: (params['up'] == 'true') ? 1 : 0,
	        	down: (params['down'] == 'true' && params['up'] == 'false') ? 1 : 0,
	        	html: render partial: 'ratings/show', locals: {debate_id: params['debate_id'].to_i}
	        }
	        format.json { render json: data.to_json }
	      else
	        format.html { render :new }
	        format.json { render json: @rating.errors, status: :unprocessable_entity }
	      end
	    end
		else
			# usuario ya ha votado
		end
		# abort
	end
end