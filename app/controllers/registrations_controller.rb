class RegistrationsController < Devise::RegistrationsController
  def new
    super
  end

  def create
    # authorize! :update, @user, message: 'Not authorized as an administrator.'
    # params.permit!
    # abort
    @user = User.new(signup_params)
    respond_to do |format|
      if @user.save
        format.html { redirect_to root_path, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
  	if account_update_params[:current_password] == ''
  		params[:user].delete(:current_password)
	    respond_to do |format|
	      if @user.update_without_password(account_update_params)
	        format.html { redirect_to root_path, notice: 'User was successfully updated.' }
	        format.json { render :show, status: :created, location: @user }
	      else
	        format.html { render :new }
	        format.json { render json: @user.errors, status: :unprocessable_entity }
	      end
	    end
  	else
	    respond_to do |format|	
	      if @user.update_with_password(devise_parameter_sanitizer.sanitize(:account_update))
	        format.html { redirect_to root_path, notice: 'User was successfully updated.' }
	        format.json { render :show, status: :created, location: @user }
	      else
	        format.html { render :new }
	        format.json { render json: @user.errors, status: :unprocessable_entity }
	      end
	    end
  	end
  end


  private
 
 	def signup_params
    params.require(:user).permit(:name, :email, :avatar, :password, :password_confirmation)
  end

  def account_update_params
    params.require(:user).permit(:name, :email, :avatar, :password, :password_confirmation, :current_password)
  end
end 