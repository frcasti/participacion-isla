class Tag < ActiveRecord::Base
	has_many :debate_tags
	has_many :debates, through: :debate_tags
end
