class Debate < ActiveRecord::Base
	include Bootsy::Container

	belongs_to :user
	has_many :ratings
	has_many :comments
	has_many :debate_tags
	has_many :tags, through: :debate_tags

	mount_uploader :featured_image, AvatarUploader
end
