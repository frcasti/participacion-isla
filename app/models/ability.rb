class Ability
  include CanCan::Ability

  def initialize(user)
    if user
      can [:read, :create], Debate
      can [:update, :delete], Debate, Debate do |debate|
        (debate.user_id == user.id) unless debate.user.nil?
      end
      can [:read, :create], Comment
      can [:update, :delete], Comment, Comment do |comment|
        (comment.user_id == user.id) unless comment.user.nil?
      end
    else
      can :read, Debate
      can :read, Comment
    end
  end
end
