class CreateDebateTags < ActiveRecord::Migration
  def change
    create_table :debate_tags do |t|
      t.integer :debate_id
      t.integer :tag_id

      t.timestamps null: false
    end
  end
end
