class AddFeaturedImageToDebate < ActiveRecord::Migration
  def change
    add_column :debates, :featured_image, :string
  end
end
