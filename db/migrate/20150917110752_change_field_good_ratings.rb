class ChangeFieldGoodRatings < ActiveRecord::Migration
  def change
  	remove_column :ratings, :boolean
  	add_column :ratings, :up, :boolean
  	add_column :ratings, :down, :boolean
  end
end
